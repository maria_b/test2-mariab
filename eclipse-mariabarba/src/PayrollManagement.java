
public class PayrollManagement {
	public static void main(String[] args) {
		//harcode values for each employee
		Employee[] employees = new Employee[5];
		employees[0]=new SalariedEmployee(20000);
		employees[1]=new HourlyEmployee(45,50);
		employees[2]=new UnionizedHourlyEmployee (17,13,2400);
		employees[3]=new SalariedEmployee(30000);
		employees[4]=new HourlyEmployee(19,20);
		 System.out.println(getTotalExpenses(employees));
		 //get total expenses for all employees
	}
	public static int  getTotalExpenses(Employee[] employees) {
		int expensesSum=0;
		for(Employee oneEmployee:employees) {
			expensesSum=expensesSum+oneEmployee.getYearlyPay(); 
		}
		return expensesSum;
		//return the amount of yearly expenses from adding all
		//the yearly salaries of the employees
	}

}
