
public class UnionizedHourlyEmployee extends HourlyEmployee {
private int pensionContribution;
public UnionizedHourlyEmployee(int numHoursWeek,int hourlyPay, int pensionContribution) {
	super(numHoursWeek,hourlyPay);
	this.pensionContribution=pensionContribution;
}
public  int getYearlyPay() {
	return super.getYearlyPay()+pensionContribution;
}

}
