
public class HourlyEmployee implements Employee{
private int numHoursWeek;
private int hourlyPay;
public HourlyEmployee(int numHoursWeek, int hourlyPay) {
	this.numHoursWeek=numHoursWeek;
	this.hourlyPay=hourlyPay;
}
public  int getYearlyPay() {
return (this.numHoursWeek*this.hourlyPay*52);	
}
public int getnumHoursWeek() {
return this.numHoursWeek;
}
public int gethourlyPay() {
return this.hourlyPay;
}
}
