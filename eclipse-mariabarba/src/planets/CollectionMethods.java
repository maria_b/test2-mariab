package planets;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		Collection<Planet> firstThreePlanets= new ArrayList<Planet>();
		for(Planet planetinColl: planets) {
			if(planetinColl.getOrder()<4 && planetinColl.getOrder()>0 ) {
				firstThreePlanets.add(planetinColl);
				//if the planet is of order either 1,2 or 3, it is added
				//to the firstThreePlanets ArrayList that will be returned
			}
			else {
				break;
				//if the planet is not one of the first three planets\
				//we break from the loop
			}
		}
		return firstThreePlanets;
		//we return the first three planets in the system
	}
}
