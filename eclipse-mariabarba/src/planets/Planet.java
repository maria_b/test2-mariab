package planets;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet> {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	@Override
	public boolean equals(Object o){
		if(!(o instanceof Planet)){
		return false;
		//if the object is not an instance of Planet,immediately return false
		}
		Planet theOtherPlanet=(Planet)o;
		if(this.name == theOtherPlanet.getName() && this.order==theOtherPlanet.getOrder()) {
			return true;
			//both planets are equal if and only if
			//both of their name and order is identical
		}
		else {
		return false ;
	}
		}
	@Override
	public int hashCode(){
		String planetEqualityValues=this.name+this.order;
		//will store planet name and order in the same string
		return planetEqualityValues.hashCode();
		//will hashcode planetEqualityValues
	}

	@Override
	public int compareTo(Planet p) {
		int planetNameComparison=this.getName().compareTo(p.getName());
		if(planetNameComparison==0){
		//if the name of both planets is the same,we compare on planetarySystemName
			if (this.getPlanetarySystemName().compareTo(p.getPlanetarySystemName())<-1){
				return 1;}
			//if the planetaryName of the this object
			//comes before the input , return a positive number so that 
			//we have a sort in decreasing order for PlanetarySystemName
			if (this.getPlanetarySystemName().compareTo(p.getPlanetarySystemName())>1){
				return -1;}
			//if the planetaryName of the this object
			//comes after the input , return a negative number so that 
			//we have a sort in decreasing order for PlanetarySystemName
			return 0;
			// if planetaryName is equal to 0,return 0 to signify that the
			//two planets are equal
		}
		return planetNameComparison;
		}
	

	
}
